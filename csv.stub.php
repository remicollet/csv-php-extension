<?php

function csv_array_to_row(array $fields, string $delimiter = ',', string $enclosure = '"', string $eolSequence = "\r\n"): string {}

function csv_row_to_array(string $row,  string $delimiter = ',', string $enclosure = '"', string $eolSequence = "\r\n"): array {}

function csv_collection_to_file(array $collection, string $delimiter = ',', string $enclosure = '"', string $eolSequence = "\r\n"): string {}

function csv_file_to_collection(string $file, string $delimiter = ',', string $enclosure = '"', string $eolSequence = "\r\n"): array {}
