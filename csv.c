/* csv extension for PHP */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h"
#include "ext/standard/php_string.h"
#include "php_csv.h"
#include "csv_arginfo.h"

#include <stdbool.h>
#include "zend_smart_str.h"

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(csv)
{
	php_info_print_table_start();
		php_info_print_table_header(2, "CSV support", "enabled");
		php_info_print_table_header(2, "Version", PHP_CSV_VERSION);
		php_info_print_table_header(2, "Author", "George Peter Banyard");
		php_info_print_table_header(2, "Bug reports", "https://gitlab.com/Girgias/csv-php-extension/issues");
	php_info_print_table_end();
}
/* }}} */

/* In PHP 8.0. zend_throw_error(NULL, ...) should be converted to zend_value_error(...) */
#define EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS() {														\
	if (eol_sequence) {																							\
		/* Make sure that there is at least one character in string */											\
		if (ZSTR_LEN(eol_sequence) < 1) {																		\
			zend_throw_error(NULL, "EOL sequence cannot be empty");												\
			return;																								\
		}																										\
	} else {																									\
		eol_sequence = zend_string_init("\r\n", strlen("\r\n"), 0);												\
	}																											\
																												\
	if (delimiter) {																							\
		/* Make sure that there is at least one character in string */											\
		if (ZSTR_LEN(delimiter) < 1) {																			\
			zend_throw_error(NULL, "Delimiter cannot be empty");												\
			zend_string_release(eol_sequence);																	\
			RETURN_THROWS();																					\
		}																										\
		if (0 == zend_binary_strcmp(ZSTR_VAL(delimiter), ZSTR_LEN(delimiter),									\
				ZSTR_VAL(eol_sequence), ZSTR_LEN(eol_sequence))) {												\
			zend_throw_error(NULL, "Delimiter cannot be identical to the EOL sequence");						\
			zend_string_release(eol_sequence);																	\
			RETURN_THROWS();																					\
		}																										\
	} else {																									\
		delimiter = zend_string_init(",", strlen(","), 0);														\
	}																											\
																												\
	if (enclosure) {																							\
		if (ZSTR_LEN(enclosure) < 1) {																			\
			zend_throw_error(NULL, "Enclosure cannot be empty");												\
			zend_string_release(delimiter);																		\
			zend_string_release(eol_sequence);																	\
			RETURN_THROWS();																					\
		}																										\
		if (0 == zend_binary_strcmp(ZSTR_VAL(enclosure), ZSTR_LEN(enclosure),									\
				ZSTR_VAL(eol_sequence), ZSTR_LEN(eol_sequence))) {												\
			zend_throw_error(NULL, "Enclosure cannot be identical to the EOL sequence");						\
			zend_string_release(eol_sequence);																	\
			zend_string_release(delimiter);																		\
			RETURN_THROWS();																					\
		}																										\
	} else {																									\
		enclosure = zend_string_init("\"", strlen("\""), 0);													\
	}																											\
																												\
	/* Ensure delimiter and enclosure are different */															\
	if (0 == zend_binary_strcmp(ZSTR_VAL(delimiter), ZSTR_LEN(delimiter), ZSTR_VAL(enclosure), ZSTR_LEN(enclosure))) {	\
		zend_throw_error(NULL, "Delimiter and enclosure cannot be identical");									\
		zend_string_release(eol_sequence);																		\
		zend_string_release(delimiter);																			\
		zend_string_release(enclosure);																			\
		RETURN_THROWS();																						\
	}																											\
}

/**
 * Follows RFC4180 https://tools.ietf.org/html/rfc4180
 * The terminology can be slightly confusing here as what PHP considers the 'enclosure' is what is used
 * to escape a field in the RFC.
 * 
 * This only formats ONE row of a CSV file.
 */
static zend_string* hashtable_to_rfc4180_string(
		HashTable *fields, const zend_string *delimiter, const zend_string *enclosure, const zend_string *eol_sequence)
{
	int nb_fields;
	int fields_iterated = 0;
	zval *tmp_field_zval;
	smart_str row = {0};
	zend_string *return_value;

	nb_fields = zend_hash_num_elements(fields);
	ZEND_HASH_FOREACH_VAL(fields, tmp_field_zval) {
		zend_string *tmp_field_str;
		zend_string *field_str = zval_get_tmp_string(tmp_field_zval, &tmp_field_str);
		int escape_field = 0;
		int enclosure_within_filed = 0;

		/*
		 * A field must be escaped (enclosed) if it contains the delimiter OR a Carriage Return (\r) 
		 * OR a Line Feed (\n) OR the field escape sequence (i.e. enclosure parameter) OR the custom EOL sequence
		 */
		if (
			memchr(ZSTR_VAL(field_str), '\n', ZSTR_LEN(field_str))
			|| memchr(ZSTR_VAL(field_str), '\r', ZSTR_LEN(field_str))
			|| php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(delimiter), ZSTR_LEN(delimiter),
				ZSTR_VAL(field_str) + ZSTR_LEN(field_str))
			|| php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(eol_sequence), ZSTR_LEN(eol_sequence),
				ZSTR_VAL(field_str) + ZSTR_LEN(field_str))
		) {
			escape_field = 1;
		}
		/* If the field escape sequence (i.e. enclosure parameter) is within the field it needs to be
		 * duplicated within the field. */
		if (php_memnstr(ZSTR_VAL(field_str), ZSTR_VAL(enclosure), ZSTR_LEN(enclosure),
			ZSTR_VAL(field_str) + ZSTR_LEN(field_str))) {
			escape_field = 1;
			enclosure_within_filed = 1;
		}

		if (escape_field) {
			smart_str_append(&row, enclosure);

			if (enclosure_within_filed) {
				/* Create replace string (twice the field escape sequence) */
				smart_str escaped_enclosure = {0};
				smart_str_append(&escaped_enclosure, enclosure);
				smart_str_append(&escaped_enclosure, enclosure);
				smart_str_0(&escaped_enclosure);

				zend_string *replace = php_str_to_str(ZSTR_VAL(field_str), ZSTR_LEN(field_str), ZSTR_VAL(enclosure),
						ZSTR_LEN(enclosure), ZSTR_VAL(escaped_enclosure.s), ZSTR_LEN(escaped_enclosure.s));
				
				smart_str_append(
					&row,
					replace
				);

				smart_str_free(&escaped_enclosure);
				zend_string_release(replace);
			} else {
				smart_str_append(&row, field_str);
			}

			smart_str_append(&row, enclosure);
		} else {
			smart_str_append(&row, field_str);
		}

		/* Only add the delimiter in between fields on the same row. */
		if (++fields_iterated != nb_fields) {
			smart_str_append(&row, delimiter);
		}

		/* Clear temporary variable */
		zend_tmp_string_release(tmp_field_str);
	} ZEND_HASH_FOREACH_END();
	/* Add the EOL sequence to indicate the end of the row. */
	smart_str_append(&row, eol_sequence);

	smart_str_0(&row);

	return_value = zend_string_init(ZSTR_VAL(row.s), ZSTR_LEN(row.s), 0);

	smart_str_free(&row);

	return return_value;
}

static zend_string* hashtable_collection_to_rfc4180_file(
		HashTable *collection, const zend_string *delimiter,
		const zend_string *enclosure, const zend_string *eol_sequence)
{
	uint index = 0;
	zval *fields;
	int nb_fields = 0;
	int old_nb_fields = -1;
	smart_str file = {0};
	zend_string *return_file_value;

	ZEND_HASH_FOREACH_VAL(collection, fields) {
		/* Check that fields is an array */
		if (Z_TYPE_P(fields) != IS_ARRAY) {
			zend_type_error("Element %d of the collection must be an array", index);
			smart_str_free(&file);
			return NULL;
		}

		/* used to check if the number of fields is equal in each iteration */
		nb_fields = zend_hash_num_elements(Z_ARRVAL_P(fields));
		if (nb_fields != old_nb_fields && old_nb_fields != -1) {
			zend_throw_error(NULL,
				"Element %d of the collection contains %d fields compared to %d fields on previous rows",
				index, nb_fields, old_nb_fields);
			smart_str_free(&file);
			return NULL;
		}
		old_nb_fields = nb_fields;
		index++;

		zend_string *result = hashtable_to_rfc4180_string(Z_ARRVAL_P(fields), delimiter, enclosure, eol_sequence);
		smart_str_append(&file, result);
		zend_string_release(result);
	} ZEND_HASH_FOREACH_END();

	smart_str_0(&file);

	return_file_value = zend_string_init(ZSTR_VAL(file.s), ZSTR_LEN(file.s), 0);

	smart_str_free(&file);

	return return_file_value;
}

static HashTable* rfc4180_string_to_hashtable(
		char **buffer, const size_t length, const zend_string *delimiter,
		const zend_string *enclosure, const zend_string *eol_sequence)
{
	HashTable *return_value = zend_new_array(8);

	bool in_escaped_field = false;
	
	/* Dereference buffer */
	char *row = *buffer;

	smart_str field_value = {0};
	size_t field_value_length = 0;

	/* Main loop to "tokenize" the row */
	for (size_t index = 0; index < length; ++index) {
		/* Check for field escape sequence (i.e. enclosure) */
		if (php_memnstr(row, ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), row + ZSTR_LEN(enclosure))) {
			row += ZSTR_LEN(enclosure);

			if (!in_escaped_field) {
				in_escaped_field = true;
				continue;
			}

			/* In an escaped field */
			if (php_memnstr(row, ZSTR_VAL(enclosure), ZSTR_LEN(enclosure), row + ZSTR_LEN(enclosure))) {
				in_escaped_field = true;
				goto consume;
			}

			in_escaped_field = false;
			continue;
		}

		/* Check for delimiter if not in an escaped field */
		if (
			!in_escaped_field
			&& php_memnstr(
				row,
				ZSTR_VAL(delimiter),
				ZSTR_LEN(delimiter),
				row + ZSTR_LEN(delimiter)
			)
		) {
			row += ZSTR_LEN(delimiter);

			/* Add nul terminating byte */
			smart_str_0(&field_value);
			zval tmp;
			ZVAL_STRINGL(&tmp, ZSTR_VAL(field_value.s), field_value_length);
			zend_hash_next_index_insert(return_value, &tmp);

			smart_str_free(&field_value);

			field_value_length = 0;
			continue;
		}
		
		/* Check for End Of Line sequence when not in an escaped field */
		if (!in_escaped_field && php_memnstr(row, ZSTR_VAL(eol_sequence), ZSTR_LEN(eol_sequence),
				row + ZSTR_LEN(eol_sequence))) {
			row += ZSTR_LEN(eol_sequence);
			goto eol;
		}

		consume:

		smart_str_appendc(&field_value, row[0]);
		field_value_length++;
		row++;
	}
	
	eol:
	/* Add nul terminating byte */
	smart_str_0(&field_value);
	zval tmp;
	ZVAL_STRINGL(&tmp, ZSTR_VAL(field_value.s), field_value_length);
	zend_hash_next_index_insert(return_value, &tmp);

	smart_str_free(&field_value);
	/* Update outer buffer position */
	*buffer = row;
	
	return return_value;
}

static HashTable* rfc4180_file_to_hashtable_collection(
		zend_string *file, const zend_string *delimiter, const zend_string *enclosure, const zend_string *eol_sequence)
{
	HashTable *return_value = zend_new_array(8);
	
	char *start_position = ZSTR_VAL(file);
	char *current_position = ZSTR_VAL(file);
	size_t length = ZSTR_LEN(file);
	uint index = 1;
	int nb_fields = 0;
	int old_nb_fields = -1;
	
	while (current_position - start_position < (ptrdiff_t) length) {
		HashTable *row = rfc4180_string_to_hashtable(&current_position, length, delimiter, enclosure, eol_sequence);

		/* used to check if the number of fields is equal in each iteration */
		nb_fields = zend_hash_num_elements(row);
		if (nb_fields != old_nb_fields && old_nb_fields != -1) {
			zend_throw_error(NULL,
				"File row %d contains %d fields compared to %d fields on previous rows",
				index, nb_fields, old_nb_fields);
			zend_hash_destroy(row);
			zend_hash_destroy(return_value);
			FREE_HASHTABLE(row);
			FREE_HASHTABLE(return_value);
			return NULL;
		}
		old_nb_fields = nb_fields;
		index++;
		zval tmp;
		ZVAL_ARR(&tmp, row);
		zend_hash_next_index_insert(return_value, &tmp);
	}

	return return_value;
}

PHP_FUNCTION(csv_array_to_row)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	HashTable *fields;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "h|SSS", &fields, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS();

	RETVAL_STR(hashtable_to_rfc4180_string(fields, delimiter, enclosure, eol_sequence));
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);
}

PHP_FUNCTION(csv_collection_to_file)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	HashTable *collection;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "h|SSS", &collection, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS();

	zend_string *file = hashtable_collection_to_rfc4180_file(collection, delimiter, enclosure, eol_sequence);

	/* Release strings */
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);
	/* If an Error has been thrown */
	if (file == NULL) {
		RETURN_THROWS();
	}
	RETVAL_STR(file);
}

PHP_FUNCTION(csv_row_to_array)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	zend_string *row;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "S|SSS", &row, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS();
	
	char* row_c = ZSTR_VAL(row);
	RETVAL_ARR(rfc4180_string_to_hashtable(&row_c, ZSTR_LEN(row), delimiter, enclosure, eol_sequence));
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);
}

PHP_FUNCTION(csv_file_to_collection)
{
	zend_string *delimiter = NULL;
	zend_string *enclosure = NULL;
	zend_string *eol_sequence = NULL;
	zend_string *file;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "S|SSS", &file, &delimiter, &enclosure, &eol_sequence) == FAILURE) {
		RETURN_THROWS();
	}

	EOL_SEQUENCE_AND_DELIMITER_AND_ENCLOSURE_CHECKS();

	HashTable *collection = rfc4180_file_to_hashtable_collection(file, delimiter, enclosure, eol_sequence);

	/* Release strings */
	zend_string_release(eol_sequence);
	zend_string_release(delimiter);
	zend_string_release(enclosure);
	/* If an Error has been thrown */
	if (collection == NULL) {
		RETURN_THROWS();
	}
	RETVAL_ARR(collection);
}

/* {{{ csv_functions[]
 */
static const zend_function_entry csv_functions[] = {
	PHP_FE(csv_array_to_row,		arginfo_csv_array_to_row)
	PHP_FE(csv_collection_to_file,	arginfo_csv_collection_to_file)
	PHP_FE(csv_file_to_collection,	arginfo_csv_file_to_collection)
	PHP_FE(csv_row_to_array,		arginfo_csv_row_to_array)
	PHP_FE_END
};
/* }}} */

/* {{{ csv_module_entry
 */
zend_module_entry csv_module_entry = {
	STANDARD_MODULE_HEADER,
	"csv",					/* Extension name */
	csv_functions,			/* zend_function_entry */
	NULL,					/* PHP_MINIT - Module initialization */
	NULL,					/* PHP_MSHUTDOWN - Module shutdown */
	NULL,					/* PHP_RINIT - Request initialization */
	NULL,					/* PHP_RSHUTDOWN - Request shutdown */
	PHP_MINFO(csv),			/* PHP_MINFO - Module info */
	PHP_CSV_VERSION,		/* Version */
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_CSV
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(csv)
#endif
