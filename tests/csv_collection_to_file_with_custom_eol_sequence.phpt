--TEST--
Test csv_collection_to_file() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

$output = "One,Two,ThreeあUn,Deux,TroisあIchi,Ni,Sanあ";

var_dump($output === csv_collection_to_file($collection, ',', '"', 'あ'));
var_dump(csv_collection_to_file($collection, ',', '"', 'あ'));

?>
--EXPECT--
bool(true)
string(46) "One,Two,ThreeあUn,Deux,TroisあIchi,Ni,Sanあ"
