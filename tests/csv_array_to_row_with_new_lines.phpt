--TEST--
Test csv_array_to_row() with new lines
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$fields = [
    "I have a\n new line",
    "I have a\r carriage return",
    "I use a carriage\r\n return new line",
    "I don't have a new line",
];

$output = "\"I have a\n new line\",\"I have a\r carriage return\",\"I use a carriage\r\n return new line\",I don't have a new line\r\n";

var_dump($output === csv_array_to_row($fields));
var_dump(csv_array_to_row($fields));
var_dump(csv_array_to_row($fields, ','));
var_dump(csv_array_to_row($fields, ',', '"'));

?>
--EXPECT--
bool(true)
string(111) ""I have a
 new line","I have a carriage return","I use a carriage
 return new line",I don't have a new line
"
string(111) ""I have a
 new line","I have a carriage return","I use a carriage
 return new line",I don't have a new line
"
string(111) ""I have a
 new line","I have a carriage return","I use a carriage
 return new line",I don't have a new line
"
