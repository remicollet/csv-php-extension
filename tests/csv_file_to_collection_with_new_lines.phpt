--TEST--
Test csv_file_to_collection() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        "I have a\n new line",
        'Three',
    ],
    [
        'Un',
        "I have a\r carriage return",
        'Trois',
    ],
    [
        'Ichi',
        "I have a carriage\r\n return new line",
        'San',
    ],
];

$string = "One,\"I have a\n new line\",Three\r\nUn,\"I have a\r carriage return\",Trois\r\nIchi,\"I have a carriage\r\n return new line\",San\r\n";

var_dump($collection === csv_file_to_collection($string));
var_dump(csv_file_to_collection($string));

?>
--EXPECT--
bool(true)
array(3) {
  [0]=>
  array(3) {
    [0]=>
    string(3) "One"
    [1]=>
    string(18) "I have a
 new line"
    [2]=>
    string(5) "Three"
  }
  [1]=>
  array(3) {
    [0]=>
    string(2) "Un"
    [1]=>
    string(25) "I have a carriage return"
    [2]=>
    string(5) "Trois"
  }
  [2]=>
  array(3) {
    [0]=>
    string(4) "Ichi"
    [1]=>
    string(35) "I have a carriage
 return new line"
    [2]=>
    string(3) "San"
  }
}
