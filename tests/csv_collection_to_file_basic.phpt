--TEST--
Test csv_collection_to_file() with standard parameters
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois',
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

$output = "One,Two,Three\r\nUn,Deux,Trois\r\nIchi,Ni,San\r\n";

var_dump($output === csv_collection_to_file($collection));
var_dump(csv_collection_to_file($collection));
var_dump(csv_collection_to_file($collection, ','));
var_dump(csv_collection_to_file($collection, ',', '"'));
var_dump(csv_collection_to_file($collection, ',', '"', "\r\n"));

?>
--EXPECT--
bool(true)
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
string(43) "One,Two,Three
Un,Deux,Trois
Ichi,Ni,San
"
