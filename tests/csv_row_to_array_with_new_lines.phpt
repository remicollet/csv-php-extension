--TEST--
Test csv_array_to_row() with new lines
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$fields = [
    "I have a\n new line",
    "I have a\r carriage return",
    "I use a carriage\r\n return new line",
    "I don't have a new line",
];

$string = "\"I have a\n new line\",\"I have a\r carriage return\",\"I use a carriage\r\n return new line\",I don't have a new line\r\n";

var_dump($fields === csv_row_to_array($string));
var_dump(csv_row_to_array($string));
var_dump(csv_row_to_array($string, ','));
var_dump(csv_row_to_array($string, ',', '"'));

?>
--EXPECT--
bool(true)
array(4) {
  [0]=>
  string(18) "I have a
 new line"
  [1]=>
  string(25) "I have a carriage return"
  [2]=>
  string(34) "I use a carriage
 return new line"
  [3]=>
  string(23) "I don't have a new line"
}
array(4) {
  [0]=>
  string(18) "I have a
 new line"
  [1]=>
  string(25) "I have a carriage return"
  [2]=>
  string(34) "I use a carriage
 return new line"
  [3]=>
  string(23) "I don't have a new line"
}
array(4) {
  [0]=>
  string(18) "I have a
 new line"
  [1]=>
  string(25) "I have a carriage return"
  [2]=>
  string(34) "I use a carriage
 return new line"
  [3]=>
  string(23) "I don't have a new line"
}
