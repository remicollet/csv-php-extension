--TEST--
Error conditions for csv_file_to_collection()
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$string = "One,Two,Three\r\nUn,Deux\r\nIchi,Ni,San\r\n";

// Empty delimiter or enclosure
try {
    var_dump(csv_file_to_collection($string));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

?>
--EXPECT--
File row 2 contains 2 fields compared to 3 fields on previous rows
