--TEST--
Error conditions for csv_collection_to_file()
--SKIPIF--
<?php
if (!extension_loaded('csv')) {
	echo 'skip';
}
?>
--FILE--
<?php

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    25,
    [
        'Ichi',
        'Ni',
        'San',
    ],
];

try {
    var_dump(csv_collection_to_file($collection));
} catch (\TypeError $e) {
    echo $e->getMessage() . \PHP_EOL;
}

$collection = [
    [
        'One',
        'Two',
        'Three',
    ],
    [
        'Un',
        'Deux',
        'Trois'
    ],
    [
        'Ichi',
        'Ni',
        'San',
    ],
    [
        'Eins',
        'Zwei',
    ],
];

try {
    var_dump(csv_collection_to_file($collection));
} catch (\Error $e) {
    echo $e->getMessage() . \PHP_EOL;
}

?>
--EXPECT--
Element 1 of the collection must be an array
Element 3 of the collection contains 2 fields compared to 3 fields on previous rows
